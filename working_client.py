import pika
import sys 
import uuid
import pickle
import json 

#working means work in progress 

name="The Adventures of Huckleberry Finn"
author="Mark Twain"


#that is weird, it did not send the strig 


#above code implements distributed tasks among workers in sequence using RabbitMQ

class BookInfo(object):
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))

        self.channel = self.connection.channel()
        self.name=name
        self.author=author
        #depending on the command, need to construct a payload to be sent to the processor 
        self.payload =        {  
            'Action':'ADD',
            'Msg': { 'Book Info': {
                    'Name': self.name,
                    'Author':self.author,
                    }
                  }
        } #there is some syntax error here
        self.stringPayload=pickle.dumps(self.payload)  
        

        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(self.on_response, no_ack=True,
                                   queue=self.callback_queue)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body
            

    def call(self):
        print("Sent payload:") 
        print(self.stringPayload) 
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='',
                                   routing_key='rpc_queue',
                                   properties=pika.BasicProperties(
                                         reply_to = self.callback_queue,
                                         correlation_id = self.corr_id,
                                         ),
                                   body=self.stringPayload) #unhashable type: slice error 
        while self.response is None:
            self.connection.process_data_events()
        return self.response
    
   


book_rpc=BookInfo() 
#depending on the commmand, change this print 
print(" [x] Requesting to add a book")
print(" [x] Sending" % book_rpc.payload)
#response = fibonacci_rpc.call(30) #does the book exist (from the processor)
response = book_rpc.call()

print(" [.] Got %r" % pickle.dumps(response))

'''channel.basic_qos(prefetch_count=1)
channel.basic_consume(on_request, queue='rpc_queue')

print(" [x] Awaiting RPC requests")
channel.start_consuming()'''


