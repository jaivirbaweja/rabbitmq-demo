import pika
import sys
import time
import pickle 


connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))

channel = connection.channel()

channel.queue_declare(queue='rpc_queue')

payload= {  
            'Action':'ADD',
            'Msg': { 'Book Info': {
                    'Name': '5=5',
                    'Author':'arent i good at math',
                    }
                  }
        } #there is some syntax error here

def returnResponse():
    return pickle.dumps(payload) 
    #if add book, then return x
    #elif count return the current book count
    #etc...

def on_request(ch, method, props, body):
    response=returnResponse()
    
    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                         props.correlation_id),
                     body=response)
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(on_request, queue='rpc_queue')

print(" [x] Awaiting RPC requests")
channel.start_consuming()










